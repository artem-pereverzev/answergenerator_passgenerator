#### Made by andrei.tymoshchuk.gdc@ts.fujitsu.com for friends :) ####
#### Простая генерация ответов из шаблона + генератор паролей ####
#### 31.12.2020

import string
import random
import subprocess
import os

# Functions. Start.


def password_generator_user(length=8):
    """Generate password for user"""
    printable = f'{LETTERS}{NUMBERS}'
    printable = list(printable)
    random.shuffle(printable)
    random_password = random.choices(printable, k=length)
    random_password = ''.join(random_password)
    return random_password


def password_generator_archive(length=20):
    """Generate password for zip-archive"""
    printable = f'{LETTERS}{NUMBERS}'
    printable = list(printable)
    random.shuffle(printable)
    random_password = random.choices(printable, k=length)
    random_password = ''.join(random_password)
    return random_password


def account_add_group(): #1
    """Adding account to group"""
    account_name = input("Enter account name: ")
    group_name = input("Enter group name: ")
    print("Hello!" + '\n' + "Account " + account_name + " has been added to the " + group_name + "." + replication_notify + '\n' + bye_notify)


def account_del_group(): #2
    """Deleting account from group"""
    account_name = input("Enter account name: ")
    group_name = input("Enter group name: ")
    print("Hello!" + '\n' + "Account " + account_name + " was remove from " + group_name + "." + replication_notify + '\n' + bye_notify)


def usb_exclusion_add(): #3
    computer_name = input("Enter computer name: ")
    print("Hello!" + '\n' + "Access has been granted." + '\n' + "The computer " + computer_name + " has been added to the PG-Exclusion_Deny_USB_Write-XL group."
          + replication_notify + '\n' + bye_notify)


def usb_exclusion_del(): #4
    computer_name = input("Enter computer name: ")
    print("Hello!" + '\n' + '\n' + "The computer " + computer_name + " has been deleted from the PG-Exclusion_Deny_USB_Write-XL group."
          + replication_notify + '\n' + bye_notify)


def create_user_admin(): #5
    account_name = input("Enter account OneID: ")
    full_ac_name = input("Enter full name account: ")
    email_account = input("Enter account email: ")
    password_user = password_generator_user(12)
    password_archive = password_generator_archive()
    print("Password for user " + account_name + ": " + password_user)
    print("Password for zip-archive: " + password_archive)
    create_result = open(full_ac_name + '(!)' + '.txt', 'w')
    create_result.write("Account type: Global admin account" + '\n' +
                        "Full name: " + full_ac_name + '\n' +
                        "Login: " + account_name + "@bat.com" + '\n' +
                        "Password: " + str(password_user))
    create_result.close()
    archive_name = full_ac_name + '(!)' + " Password for ZIP Archive Decryption"
    create_zip = open(archive_name + '.txt', 'w')
    create_zip.write(password_archive)
    create_zip.close()
    do_make_zip = subprocess.check_output(['C:\\Program Files\\7-Zip\\7z.exe', 'a', '-p'+password_archive, '-y', full_ac_name + '(!)' + '.zip'] +
                                  [full_ac_name + '(!)' + '.txt'])
    print('\n' + "Hello!" + '\n' + "Account " + account_name + " has been successfully created." + '\n' + "Login\Pass were sent to "
          + email_account + bye_notify)
    print('\n' + "!!! FOR EMAIL LETTER !!!" + '\n' + "this E-Mail Contains only Encrypted Archive with Login\Pass from Your Global Account. "
                                              "Key for Archive Decryption - we will Send You in the Next E-Mail." + '\n' + "this E-Mail contains only Key for Archive Decryption "
                                                                                                                           "from Previous E-Mail")
    os.remove(full_ac_name + '(!)' + '.txt')


def create_svc_account(): #6
    full_ac_name = input("Enter full name account: ")
    email_account = input("Enter email for send account: ")
    password_user = password_generator_user(25)
    password_archive = password_generator_archive()
    print("Password for user " + full_ac_name + ": " + password_user)
    print("Password for zip-archive: " + password_archive)
    create_result = open(full_ac_name + '.txt', 'w')
    create_result.write("Account type: Service Account" + '\n' +
                        "Full name: " + full_ac_name + '\n' +
                        "Login: " + full_ac_name + "@bat.com"+ '\n' +
                        "Password: " + str(password_user))
    create_result.close()
    archive_name = full_ac_name + " Password for ZIP Archive Decryption"
    create_zip = open(archive_name + '.txt', 'w')
    create_zip.write(password_archive)
    create_zip.close()
    do_make_zip = subprocess.check_output(['C:\\Program Files\\7-Zip\\7z.exe', 'a', '-p'+password_archive, '-y', full_ac_name + '.zip'] +
                                  [full_ac_name + '.txt'])
    print('\n' + "Hello!" + '\n' + "The service account " + full_ac_name + " has been successfully created." + '\n' + "Login\Pass were sent to "
          + email_account + bye_notify)
    print('\n' + "!!! FOR EMAIL LETTER !!!" + '\n' + "this E-Mail Contains only Encrypted Archive with Login\Pass from Your Global Account. "
                                              "Key for Archive Decryption - we will Send You in the Next E-Mail." + '\n' + "this E-Mail contains only Key for Archive Decryption "
                                                                                                                           "from Previous E-Mail")
    os.remove(full_ac_name + '.txt')


def create_group(): #7
    group_name = input("Enter group name: ")
    print("Hello!" + '\n' + "New group " + group_name + " successfully created." + replication_notify + bye_notify)


def reset_adm_password(): #8
    account_name = input("Enter account OneID: ")
    full_ac_name = input("Enter full name account: ")
    email_account = input("Enter account email: ")
    password_user = password_generator_user(12)
    password_archive = password_generator_archive()
    print("Password for user " + account_name + ": " + password_user)
    print("Password for zip-archive: " + password_archive)
    create_result = open(full_ac_name + '(!)' + '.txt', 'w')
    create_result.write("Account type: Global admin account" + '\n' +
                        "Full name: " + full_ac_name + '\n' +
                        "Login: " + account_name + "@bat.com" '\n' +
                        "Password: " + str(password_user))
    create_result.close()
    archive_name = full_ac_name + '(!)' + " Password for ZIP Archive Decryption"
    create_zip = open(archive_name + '.txt', 'w')
    create_zip.write(password_archive)
    create_zip.close()
    do_make_zip = subprocess.check_output(['C:\\Program Files\\7-Zip\\7z.exe', 'a', '-p'+password_archive, '-y', full_ac_name + '(!)' + '.zip'] +
                                  [full_ac_name + '(!)' + '.txt'])
    print('\n' + "Hello!" + '\n' + "Password for account Global\\" + account_name + " has been successfully reset." + '\n' + "Login\Pass were sent to "
          + email_account + bye_notify)
    print('\n' + "!!! FOR EMAIL LETTER !!!" + '\n' + "this E-Mail Contains only Encrypted Archive with Login\Pass from Your Global Account. "
                                              "Key for Archive Decryption - we will Send You in the Next E-Mail." + '\n' + "this E-Mail contains only Key for Archive Decryption "
                                                                                                                           "from Previous E-Mail")
    os.remove(full_ac_name + '(!)' + '.txt')


def starting_generator():
    """Start generator and select function"""
    print("1 - Add account to group." + '\n' +
          "2 - Delete account from group." + '\n' +
          "3 - Add USB Exclusion." + '\n' +
          "4 - Remove USB Exclusion." + '\n' +
          "5 - Create admin (!) account." + '\n' +
          "6 - Create SvC account." + '\n' +
          "7 - Create group." + '\n' +
          "8 - Reset admin (!) password.")
    select_option = input("So, what we gonna do: ")
    if select_option == str(1):
        account_add_group()
    if select_option == str(2):
        account_del_group()
    if select_option == str(3):
        usb_exclusion_add()
    if select_option == str(4):
        usb_exclusion_del()
    if select_option == str(5):
        create_user_admin()
    if select_option == str(6):
        create_svc_account()
    if select_option == str(7):
        create_group()
    if select_option == str(8):
        reset_adm_password()

# Functions. End.

# Variables. Start.
replication_notify = '\n' + "Please be aware - it takes up to 3 hours to complete the replication of the changes and then re-login" \
                     " is required for changes to take effect."
bye_notify = '\n' + '\n' + "Best Regards, AD Team"
LETTERS = string.ascii_letters  # for password generator
NUMBERS = string.digits  # for password generator
# Variables. End.


# LET THAT SHIT IS WORK

starting_generator()
exit_question = input('\n' + "All jobs done. Press any key to exit or 1 for repeat: ")

while exit_question == str(1):
    starting_generator()
    exit_question = input('\n' + "All jobs done. Press any key to exit or 1 for repeat: ")
else:
    print("SEE YA SOON")




